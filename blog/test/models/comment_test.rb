require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  test "successfully make a comment" do
    comment = Comment.create(username: "Name", body: "Some comment.")
    assert_instance_of(Comment, comment, msg="Not a comment object.")
  end

  test "ensure that a comment with a username is stored correctly" do
    comment = Comment.create(username: "Name", body: "Some comment.")
    assert_equal(comment.username, "Name", msg="Username not stored correctly.")
  end

  test "ensure that a comment with a body is stored correctly" do
    comment = Comment.create(username: "Name", body: "Some comment.")
    assert_equal(comment.body, "Some comment.", msg="Body not stored correctly.")
  end

  test "add a comment as a reply to a comment" do
    comment = Comment.create(username: "Name", body: "Some comment.")
    assert_equal(nil, comment.comments, msg="Body not stored correctly.")
    comment.comments.create(username: "Name", body: "Another comment.")
    assert_instance_of(Comment, comment.comment, msg="Comment not there.")
  end
end
