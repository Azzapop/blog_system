require 'test_helper'

class PostTest < ActiveSupport::TestCase
  test "successfully make a post" do
    post = Post.create(title: "Blog Post", body: "Some information.")
    assert_instance_of(Post, post, msg="Not a Post object.")
  end

  test "ensure that a post with a title is stored correctly" do
    post = Post.create(title: "Blog Post", body: "Some information.")
    assert_equal(post.title, "Blog Post", msg="Title not stored correctly")
  end

  test "ensure that a post with a body is stored correctly" do
    post = Post.create(title: "Blog Post", body: "Some information.")
    assert_equal(post.body, "Some information.", msg="Title not stored correctly")
  end

  test "a post can have 0 or more comments" do
    post = Post.create(title: "Blog Post", body: "Post body.")
    assert_equal(0, post.comments.count, msg="Post comments do not exist")
    post.comments.create(username: "Name", body: "Comment body.")
    assert_equal(1, post.comments.count, msg="Comment not added to post.")
  end
end
