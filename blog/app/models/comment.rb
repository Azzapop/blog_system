class Comment < ActiveRecord::Base
    belongs_to  :post

    belongs_to :reply, class_name: "Comment"
end
