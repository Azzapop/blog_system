class BlogController < ApplicationController
  def home
  end

  def post_index
    @posts = Post.all
  end

  def show_post
    @post = Post.find(params[:id])
    @comment = Comment.new
  end

  def feedback
  end

  def add_comment
    post = Post.find(params[:post_id])
    if params[:type] == "comment"
      post.comments.create(comment_params)
      redirect_to post_path :id => post.id
    else
      comment = Comment.find(params[:comment_id])
      reply = Comment.create(comment_params)
      comment.reply = reply
      comment.save
      redirect_to post_path :id => params[:post_id]
    end
  end

  def add_reply
  end

  private
  def comment_params
    params.require(:comment).permit(:username, :body)
  end
end
