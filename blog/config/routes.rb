Rails.application.routes.draw do
  root 'blog#home'

  get 'posts'         => 'blog#post_index'
  get 'post/:id'      => 'blog#show_post', as: 'post'
  get 'feedback'      => 'blog#feedback'
  post 'post'         => 'blog#add_comment'
end
