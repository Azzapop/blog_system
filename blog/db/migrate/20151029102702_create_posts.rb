class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title, default: "<No Title>"
      t.text :body, default: "<No Body>"

      t.timestamps null: false
    end
  end
end
