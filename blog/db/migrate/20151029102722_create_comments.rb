class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.belongs_to :post
      t.belongs_to :reply
      t.string :username, default: "<Anonymous>"
      t.text :body, default: "<No Comment>"

      t.timestamps null: false
    end
  end
end
