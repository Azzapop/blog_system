# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


post1 = Post.create(title: "Title1", body: "Body1")
post2 = Post.create(title: "Title2", body: "Body2")
post3 = Post.create(title: "Title3", body: "Body3")
post4 = Post.create(title: "Title4", body: "Body4")
post1comment1 = post1.comments.create(username: "Name1", body: "Body1")
post1.comments.create(username: "Name2", body: "Body2")
post1.comments.create(username: "Name3", body: "Body3")
post1.comments.create(username: "Name4", body: "Body4")
post2.comments.create(username: "Name1", body: "Body1")
post2.comments.create(username: "Name2", body: "Body2")
post2.comments.create(username: "Name3", body: "Body3")
post2.comments.create(username: "Name4", body: "Body4")
post3.comments.create(username: "Name1", body: "Body1")
post3.comments.create(username: "Name2", body: "Body2")
post3.comments.create(username: "Name3", body: "Body3")
post3.comments.create(username: "Name4", body: "Body4")
post4.comments.create(username: "Name1", body: "Body1")
post4.comments.create(username: "Name2", body: "Body2")
post4.comments.create(username: "Name3", body: "Body3")
post4.comments.create(username: "Name4", body: "Body4")

comment2 = Comment.create(username: "Name2", body: "Body2")
post1comment1.reply = comment2
post1comment1.save
